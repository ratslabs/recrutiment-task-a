# Geolocation Application

## Summary
The Geolocation Application is a desktop application that allows storing and retrieving geolocation data based on IP addresses or URLs. It provides an interface to interact with the application and manage the geolocation data stored in a database.

## Features
- Retrieve geolocation data based on IP addresses or URLs.
- Store geolocation data in the database.
- User-friendly interface implemented using ReactiveUI.
- Utilize the IPStack API (https://ipstack.com/) for geolocation data retrieval.
- Built on .NET 6 and Windows platform.
- Leveraging the Entity Framework and SQL Server for database operations.
- Incorporates unit testing using xUnit framework for ensuring code quality.
- The application is built with a layered architecture to separate concerns and promote maintainability.

## Getting Started
To run the application locally and set up the development environment, follow these steps:

1. Clone the repository: `git clone [repository-url]`
2. Install the required dependencies.
3. Configure the connection to the SQL Server database.
4. Run the application.

## Testing
Unit tests have been implemented to ensure the functionality and stability of the application. The tests cover the following scenarios:
- Testing the custom HttpClient for retrieving geolocation data from the IPStack API.
- Verifying the database service's ability to store data and return the expected number of records.

To run the tests, execute the following command: `dotnet test`

## Contribution
Contributions are welcome! If you would like to contribute to the Geolocation Application, please follow the guidelines in the CONTRIBUTING.md file.

## License
This project is licensed under the [License Name] - see the LICENSE.md file for details.

## Contact
If you have any questions or suggestions regarding the Geolocation Application, please feel free to reach out to us at [contact-email].
