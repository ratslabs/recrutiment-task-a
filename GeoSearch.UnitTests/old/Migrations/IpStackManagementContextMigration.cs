﻿//using Microsoft.EntityFrameworkCore.Infrastructure;
//using Microsoft.EntityFrameworkCore.Migrations;

//namespace GeoSearch.Migrations;

//[Migration("1_VehicleContextmigration")]
//[DbContext(typeof(IpStackManagementContextMigration))]
//public class IpStackManagementContextMigration : Migration
//{
//    protected override void Up(MigrationBuilder migrationBuilder)
//    {
//        migrationBuilder.CreateTable(
//            name: "IpAddressDetails",
//            columns: table => new
//            {
//                Id = table.Column<int>(type: "int", nullable: false)
//                    .Annotation("SqlServer:Identity", "1, 1"), /// <- google it and then you are going to know how to do it , it is not going to be that hard ; )
                
//                Ip = table.Column<string>(nullable: true),
//                Hostname = table.Column<string>(nullable: true),
//                Type = table.Column<string>(nullable: true),
//                ContinentCode = table.Column<string>(nullable: true),
//                ContinentName = table.Column<string>(nullable: true),
//                CountryCode = table.Column<string>(nullable: true),
//                CountryName = table.Column<string>(nullable: true),
//                RegionCode = table.Column<string>(nullable: true),
//                RegionName = table.Column<string>(nullable: true),
//                City = table.Column<string>(nullable: true),
//                Zip = table.Column<string>(nullable: true),
//                Latitude = table.Column<double>(nullable: false),
//                Longitude = table.Column<double>(nullable: false),
//            },
//            constraints: table =>
//            {
//                table.PrimaryKey("PK_IpAddresDetails", x => x.Id);
//            });


//        //migrationBuilder.CreateTable(
//        //    name: "Locations",
//        //    columns: table => new
//        //    {
//        //        Id = table.Column<int>(type: "int", nullable: false)
//        //            .Annotation("SqlServerIdentity", "1, 1"),

//        //        GeonameId = table.Column<string>(type: "string", nullable: false),
//        //        Capital = table.Column<string>(type: "string", nullable: false),
//        //        //Languages = table.Column<string>(type: "string", nullable: false),
//        //        CountryFlag = table.Column<string>(type: "string", nullable: false),
//        //        CountryFlagEmoji = table.Column<string>(type: "string", nullable: false),
//        //        CountryFlagEmojiUnicode = table.Column<string>(type: "string", nullable: false),
//        //        CallingCode = table.Column<string>(type: "string", nullable: false),
//        //        IsEu = table.Column<bool>(type: "bool", nullable: false),
//        //    },
//        //    constraints: table =>
//        //    {
//        //        table.PrimaryKey("PK_Location", x => x.Id);
//        //    });
//        //migrationBuilder.CreateTable(
//        //    name: " ")
//    }


//    protected override void Down(MigrationBuilder migrationBuilder)
//    {
//        migrationBuilder.DropTable("IpAddressDetails");
//        //migrationBuilder.DropTable("Locations");
//    }

//}
