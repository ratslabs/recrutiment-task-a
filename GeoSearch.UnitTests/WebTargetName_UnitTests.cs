﻿using GeoSearch.IpStackManagement;
using System.Text.RegularExpressions;

namespace GeoSearch.UnitTests;

public class WebTargetName_UnitTests
{
    
    [Fact] public void IsIPV4alid_ValidInput_ReturnsTrue()
    {
        bool validIp = IpOrUri.IsValid("1.1.1.1");
        bool validUri = IpOrUri.IsValid("fsd8.io/sf");

        Assert.True(validIp);
        Assert.True(validUri);
    }

    [Fact] public void IsIPV4alid_InvalidInput_ReturnsFalse()
    {
        bool wrongIp = IpOrUri.IsValid("324.1.1.1");
        bool wrongUrl = IpOrUri.IsValid("com.2pl");

        Assert.False(wrongIp);
        Assert.False(wrongUrl);
    }

}
