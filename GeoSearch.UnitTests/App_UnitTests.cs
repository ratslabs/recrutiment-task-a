﻿//using Castle.Core.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GeoSearch.UnitTests;

public class AppHttClient_UnitTests {

    private class JsonPlaceholderRequest { }

    /// <summary> Method is testing AppHttClient as much as it tests if jsonplaceholder web-api is up .</summary>
    [Fact] public async Task GetOKFromAppHttpClient_BingUri_ReturnsOK() 
    {
        var webClient = new AppHttpClient();
        var uri = "https://jsonplaceholder.typicode.com/posts";

        (IEnumerable<JsonPlaceholderRequest> result, bool succes) 
            = await webClient.GetAsync<IEnumerable<JsonPlaceholderRequest>>(uri);

        Assert.True(succes);
    }
}


public class GeolocalisationService_Tests
{
    ILogger log = LoggerFactory.Create(
                config => { }).CreateLogger("geolocalisation-service-test");

    private IpAddressDetails FakeFactory_CreateValidIpAddressDetails()
    {
        var result = IpAddressDetails.Create(IpOrUri.Create("8.8.8.8"));
        result.ContinentCode = "EU";
        result.ContinentCode = "Poland";
        result.City = "Warsaw";
        result.Latitude = 52.00;
        result.Longitude = 27.00;
        return result;
    }

    [Fact] public async Task AddIpAddressDetailsAsync_ValidInput_ReturnsTrue()
    {
        var options = new DbContextOptionsBuilder<IpStackManagementContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;

        using var context = new IpStackManagementContext(options);
        var service = new GeolocalisationService(context, log);
        int initialCount = service.GetAllAsync().Result.Value.ToList().Count();
        var fakeValidObj = FakeFactory_CreateValidIpAddressDetails();

        await service.AddIpAddressDetailsAsync(fakeValidObj);
        int resultCount = service.GetAllAsync().Result.Value.Count();

        Assert.Equal(initialCount + 1, resultCount);
    }



    /// <summary>
    /// This method do not covers the fields just some ...
    /// </summary>
    [Fact] public async Task AddIpAddressDetailsAsync2_ValidInput_ReturnsTrue()
    {
        var options = new DbContextOptionsBuilder<IpStackManagementContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;

        using var context = new IpStackManagementContext(options);
        var service = new GeolocalisationService(context, log);
        var fakeValidObj = FakeFactory_CreateValidIpAddressDetails();

        await service.AddIpAddressDetailsAsync(fakeValidObj);

        var result = await service.GetByIpUriAsync(fakeValidObj.IpOrUri);

        Assert.Equal(fakeValidObj.ToModel(), result.Value.ToModel());
    }


    [Fact] public async Task AddGeodataDetails_GetAllAsync_ReturnsTrue()
    {
        var options = new DbContextOptionsBuilder<IpStackManagementContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;

        using var context = new IpStackManagementContext(options);
        var service = new GeolocalisationService(context, log);
        int initialCount = service.GetAllAsync().Result.Value.Count();
        var fakeValidObj = FakeFactory_CreateValidIpAddressDetails();

        await service.AddIpAddressDetailsAsync(fakeValidObj);
        await service.AddIpAddressDetailsAsync(fakeValidObj);
        await service.AddIpAddressDetailsAsync(fakeValidObj);

        int resultCount = service.GetAllAsync().Result.Value.Count();

        Assert.Equal(initialCount + 3, resultCount);
    }

    [Fact] public async Task DeleteIpAddressDetails_ValidInput_ReturnsTrue()
    {
        var options = new DbContextOptionsBuilder<IpStackManagementContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;

        using var context = new IpStackManagementContext(options);
        var service = new GeolocalisationService(context, log);
        var initialCount = service.GetAllAsync().Result.Value.Count();
        var fakeValidObj = FakeFactory_CreateValidIpAddressDetails();

        await service.AddIpAddressDetailsAsync(fakeValidObj);
        var fakeValidObj2 = service.AddIpAddressDetailsAsync(fakeValidObj).Result.Value;
        await service.DeleteAddressDetailsByIdAsync(fakeValidObj2.Id);

        var resultCount = service.GetAllAsync().Result.Value.Count();


        Assert.Equal(initialCount + 1, resultCount);
    }


    [Fact] public async Task GetByIdAsync_ValidInput_ReturnsFalse()
    {
        var options = new DbContextOptionsBuilder<IpStackManagementContext>()
             .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
             .Options;

        using var context = new IpStackManagementContext(options);
        var service = new GeolocalisationService(context, log);
        var fakeValidObj = FakeFactory_CreateValidIpAddressDetails();

        fakeValidObj = service.AddIpAddressDetailsAsync(fakeValidObj).Result.Value;
        var actual = await service.GetByIdAsync(fakeValidObj.Id);


        Assert.False(actual.Value.Id is 0);
    }

    [Fact] public async Task GetByIpUriAsync_ValidInput_ReturnsTrue()
    {
        var options = new DbContextOptionsBuilder<IpStackManagementContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;

        using var context = new IpStackManagementContext(options);
        var service = new GeolocalisationService(context, log);
        var fakeValidObj = FakeFactory_CreateValidIpAddressDetails();

        await service.AddIpAddressDetailsAsync(fakeValidObj);
        var result = await service.GetByIpUriAsync(fakeValidObj.IpOrUri);

        Assert.True(result.IsSuccess);
    }

    /// <summary>
    /// Tought sqlite might be more suitable choice when playing with some ef OnModelCreation magic 
    /// altrough this test fails even without setup that do not make us of OnModelCreation.
    /// </summary>
    [Fact] public async Task GetByIpUriAsync_ValidInput_ReturnsTrueSqlite()
    {
        var options = new DbContextOptionsBuilder<IpStackManagementContext>()
            .UseSqlite("DataSource=:memory:")
            .Options;

        using var context = new IpStackManagementContext(options);
        var service = new GeolocalisationService(context, log);
        var fakeValidObj = FakeFactory_CreateValidIpAddressDetails();

        await service.AddIpAddressDetailsAsync(fakeValidObj);
        var result = await service.GetByIpUriAsync(fakeValidObj.IpOrUri);

        Assert.True(result.IsSuccess);
    }


}



