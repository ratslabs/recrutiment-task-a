global using Xunit;
global using GeoSearch.Domain;
global using GeoSearch.IpStackManagement;
global using GeoSearch.IpStackManagement.Data;
global using GeoSearch.IpStackManagement.DTOs;
global using GeoSearch.WebHndling;