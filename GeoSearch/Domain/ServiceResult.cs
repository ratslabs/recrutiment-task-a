﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoSearch.Domain;

public abstract record ServiceResult
{
    public static ServiceResult<T> Success<T>(T value) where T : class
        => new ServiceResult<T>(value, null);

    public static ServiceResult<T> Fail<T>(Exception error) where T : class 
        => new ServiceResult<T>(null, error);
}

public record ServiceResult<T>(T? Value, Exception? Error) where T : class
{
    public bool IsSuccess => Error is null;

}