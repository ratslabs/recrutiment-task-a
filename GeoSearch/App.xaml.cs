﻿using Microsoft.Extensions.Logging;
using Splat;
using System.Reflection;

namespace GeoSearch;

/// <summary>
/// Application main entry point , services installer .
/// </summary>
public partial class App
{
    public App()
    {

/// > UI
/// A helper method that will register all classes that derive off IViewFor into our dependency injection container. 
/// ReactiveUI uses Splat for it's dependency injection by default, but you can override this if you like.
        Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());


/// > LOG
        
        var log = LoggerFactory.Create(
            config =>
                {//->> missing write to file 
                }).CreateLogger("logger");


/// > DB
        
        IpStackManagementContext dbContext = new IpStackManagementContext();
        bool isDbUp = dbContext.Database.CanConnect();
        if (!isDbUp){
            string exMsg = "coulnt connect to db ...";
            try
            {
                // -> could inform user about it ...
                dbContext.Database.Migrate();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                // -> could inform user about it ...
            }
        }
        else
        {
            /// -> here depending on software version i guess
            Debug.Write("db is up ...");
            dbContext.Database.Migrate();
        }

        Locator.CurrentMutable.Register(() => dbContext);
        Locator.CurrentMutable.Register<IGeolocalisationService>(() => new GeolocalisationService(dbContext, log));



/// > WEB
        
        IAppHttpClient appHttpClient = new AppHttpClient();
        Locator.CurrentMutable.Register<IAppHttpClient>(() => appHttpClient);
        Locator.CurrentMutable.Register<IIpStackHandler>(() => new IpStackHandler(appHttpClient, log));


    }


}
