﻿using System.Text.RegularExpressions;

namespace GeoSearch.IpStackManagement;


public class IpOrUri
{
    private static Regex UriValidationRegex = new Regex(@"^[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$");
    private static Regex Ipv4ValidationRegex = new Regex("^((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d)\\.?\\b){4}$");

    /// <summary>
    /// I am interested in benchmarking thiese methods ;}
    /// </summary>
    private static bool IsIPV4alid(string ipAddress)
    {
        string[] parts = ipAddress.Split('.');

        bool isValid = parts.Length == 4
                       && !parts.Any(
                           x =>
                           {
                               return int.TryParse(x, out int y) && y > 255 || y < 1;
                           });
        return isValid;
    }


    private IpOrUri() { }
    private IpOrUri(string ipv4Url)
    {
        Ipv4Url = ipv4Url;
    }


    /// <summary>
    /// This value is unique and can replace id .
    /// </summary>
    public string Ipv4Url { get; private set; }
    //[Key] public int Id { get; init; }


    public static bool IsValidUri(string ipAddress)
    {
        string[] parts = ipAddress.Split('.');

        bool isValid = parts.Length >= 2;

        return isValid;
    }


    public static IpOrUri Create(string value)
    {
        if (!IsValid(value))
            throw new IpUri_InvalidFormat_Exception();

        return new IpOrUri(value);
    }

    public static bool TryCreate(string value, out IpOrUri ip)
    {
        if (!IsValid(value))
        {
            ip = null;
            return false;
        }

        ip = new IpOrUri(value);
        return true;
    }

    public static bool IsValid(string value) => IsIPV4alid(value) || UriValidationRegex.IsMatch(value);
}