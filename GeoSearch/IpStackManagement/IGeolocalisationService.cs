﻿using Mapster;
using Microsoft.Extensions.Logging;

namespace GeoSearch.IpStackManagement;

public class GeolocalisationService : IGeolocalisationService
{
    public GeolocalisationService(IpStackManagementContext dbContext, ILogger log)
    {
        _log = log;
        _context = dbContext;
    }

    private readonly ILogger _log;
    private readonly IpStackManagementContext _context;

    public async Task<ServiceResult<IEnumerable<IpAddressDetails>>> GetAllAsync()
    {
        try
        {
            IEnumerable<IpAddressDetails> ipAddressDetails = await _context.IpAddressGeoDetails.ToListAsync();

            //.GetAllIpAddressDetails(true);
            return ServiceResult.Success(ipAddressDetails);

        }
        catch (Exception ex)
        {
            _log.LogError(string.Format("{0}{1}", GetType().Name, ex.Message));
            Debug.WriteLine(string.Format("{0}{1}", GetType().Name, ex.Message));
            return ServiceResult.Fail<IEnumerable<IpAddressDetails>>(new DB_ERROR_EXCEPTION());
        }
    }

    public async Task<ServiceResult<IpAddressDetails>> GetByIpUriAsync(string uriOrIpv4)
    {
        if (!IpOrUri.TryCreate(uriOrIpv4, out var ipOrUri))
            return ServiceResult.Fail<IpAddressDetails>(new IpUri_InvalidFormat_Exception());

        //Debug.WriteLine(string.Format("{0} -> {1}", GetType().Name, UriOrIpv4));

        try
        {
            var ipAddressDetails = await _context.IpAddressDetailsWithIpUri(ipOrUri, true);


            return ipAddressDetails != null ? 
                ServiceResult.Success(ipAddressDetails) :
                ServiceResult.Fail<IpAddressDetails>(new Record_NotFound_Exeption());
        }
        catch (Exception ex)
        {
            _log.LogError(string.Format("{0}{1}", GetType().Name, ex.Message));
            Debug.WriteLine(string.Format("{0}{1}", GetType().Name, ex.Message));

            return ServiceResult.Fail<IpAddressDetails>(new DB_ERROR_EXCEPTION());
        }
    }


    public async Task<ServiceResult<IpAddressDetails>> GetByIdAsync(int id)
    {
        var ipAddressDetails = await _context.IpAddressDetailsWithId(id, true);

        return ipAddressDetails != null ?
            ServiceResult.Success(ipAddressDetails) :
            ServiceResult.Fail<IpAddressDetails>(new Record_NotFound_Exeption());
    }



    
    public async Task<ServiceResult<IpAddressDetails>> DeleteAddressDetailsByIdAsync(
        int id)
    {
        Debug.WriteLine(string.Format("{0} -> {1}", GetType().Name, id));

        try
        {
            var ipAddressDetails = await _context.IpAddressDetailsWithId(id, true);
            if (ipAddressDetails is null)
            {
                return ServiceResult.Fail<IpAddressDetails>(new Record_NotFound_Exeption());
            }

            _context.Remove(ipAddressDetails);

            await _context.SaveChangesAsync();

            return ServiceResult.Success(ipAddressDetails);
        }
        catch (Exception ex)
        {
            _log.LogError(string.Format("{0}{1}", GetType().Name, ex.Message));
            Debug.WriteLine(string.Format("{0}{1}", GetType().Name, ex.Message));

            return ServiceResult.Fail<IpAddressDetails>(new DELETE_RECORD_EXCEPTION());
        }
    }

    public Task<ServiceResult<IpAddressDetails>> AddIpAddressDetailsAsync(
        string Ip, string Hostname, string Type, string ContinentCode, string
        ContinentName, string CountryCode, string CountryName, string RegionCode, string RegionName,
        string City, string Zip, double Latitude, double Longitude)
            => throw new NotImplementedException();


    public async Task<ServiceResult<IpAddressDetails>> AddIpAddressDetailsAsync(IpAddressDetails ipAddressDetailsDto)
    {
        if (!IpOrUri.TryCreate(ipAddressDetailsDto.IpOrUri, out var webTargetName))
            return ServiceResult.Fail<IpAddressDetails>(new IpUri_InvalidFormat_Exception());

        try
        {
            var ipAddressDetails = IpAddressDetails.Create(webTargetName);

            ipAddressDetailsDto.Adapt(ipAddressDetails);

            var entity = _context.Add(ipAddressDetails);
            int countSaved = await _context.SaveChangesAsync();

            _log.LogInformation(string.Format("{0}::saved-true:{1}", GetType().Name, countSaved));
            
            return await this.GetByIpUriAsync(ipAddressDetails.IpOrUri);
        }

        catch (Exception ex)
        {
            _log.LogError(string.Format("{0}::{1}", GetType().Name, ex.InnerException, ex.Message));
            Debug.WriteLine(string.Format("{0}::{1}::{2}", GetType().Name, ex.InnerException, ex.Message));

            return ServiceResult.Fail<IpAddressDetails>(new DB_ERROR_EXCEPTION());
        }
    }

    public async Task<ServiceResult<IpAddressDetails>> UpdateAsync(IpAddressDetails source)
    {
        try
        {
            _context.Update(source);

            _ = await _context.SaveChangesAsync();

            return await this.GetByIpUriAsync(source.IpOrUri);
        }
        catch (Exception ex)
        {
            _log.LogError(string.Format("{0}::{1}", GetType().Name, ex.Message));
            Debug.WriteLine(string.Format("{0}{1}", GetType().Name, ex.Message));

            return ServiceResult.Fail<IpAddressDetails>(new DB_ERROR_EXCEPTION());
        }
    }

}

public interface IGeolocalisationService
{
    Task<ServiceResult<IEnumerable<IpAddressDetails>>> GetAllAsync();
    Task<ServiceResult<IpAddressDetails>> GetByIpUriAsync(string ipUri);
    Task<ServiceResult<IpAddressDetails>> GetByIdAsync(int id);


    Task<ServiceResult<IpAddressDetails>> AddIpAddressDetailsAsync(IpAddressDetails ipAddressDetailsDTO);
    Task<ServiceResult<IpAddressDetails>> AddIpAddressDetailsAsync(string Ip, string Hostname, string Type, 
        string ContinentCode, string ContinentName, string CountryCode, string CountryName, string RegionCode, 
        string RegionName, string City, string Zip, double Latitude, double Longitude); 

    Task<ServiceResult<IpAddressDetails>> DeleteAddressDetailsByIdAsync(int id);
    Task<ServiceResult<IpAddressDetails>> UpdateAsync(IpAddressDetails source);
}
