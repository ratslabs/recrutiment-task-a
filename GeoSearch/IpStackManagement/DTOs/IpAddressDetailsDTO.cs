﻿namespace GeoSearch.IpStackManagement.DTOs;

public record IpAddressDetailsDTO(string Ip, string Hostname, string Type, string ContinentCode, string ContinentName, string CountryCode,
    string CountryName, string RegionCode, string RegionName, string City, string Zip, double Latitude, double Longitude); //, Location Location,
//    Domain.TimeZone TimeZone, Currency Currency, Connection Connection, Security Security);

public record LocationDTO(string GeonameId, string Capital, List<Language> Languages, string CountryFlag, string CountryFlagEmoji,
    string CountryFlagEmojiUnicode, string CallingCode, bool IsEu);

public record LanguageDTO(string Code, string Name, string Native);
public record TimeZoneDTO(string Id, DateTime CurrentTime, int GmtOffset, string Code, bool IsDaylightSaving);
public record CurrencyDTO(string Code, string Name, string Plural, string Symbol, string SymbolNative);
public record ConnectionDTO(int? Asn, string Isp);
public record SecurityDTO(bool IsProxy, string ProxyType, bool IsCrawler, string CrawlerName, bool IsToor, string ThreatLevel, List<string> ThreatTypes);