﻿
namespace GeoSearch.IpStackManagement.DTOs;

public static class DTOExtensions
{
    public static IpAddressDetailsDTO ToModel(this IpAddressDetails source)
        => new IpAddressDetailsDTO(source.IpOrUri, source.Hostname, source.Type, source.ContinentCode, source.ContinentName,
            source.CountryCode, source.CountryName, source.RegionCode, source.RegionName, source.City, source.Zip, source.Latitude,
            source.Longitude);
}
