﻿using GeoSearch.IpStackManagement.DTOs;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace GeoSearch.IpStackManagement;

public class IpStackHandler : IIpStackHandler
{
    public IpStackHandler(IAppHttpClient httpClient, ILogger log)
    {
        ArgumentNullException.ThrowIfNull(httpClient);
        
        Logger = log;
        HttpClient = httpClient;
        //HttpClient.Client.DefaultRequestHeaders.Add("access_key", "application/json");
    }


    public string ApiKey { get; set; }
    public IAppHttpClient HttpClient { get; set; }
    public ILogger Logger { get; set; }


    public async Task<IpAddressDetailsDTO> GetGeodataAsync(string ipAddress)
    {
        IpAddressDetailsDTO response = default;
        bool success = false;

        try
        {
            var query = new Dictionary<string, string>()
            {
                ["access_key"] = ApiKey,
            };

            var uri = QueryHelpers.AddQueryString(string.Format("http://api.ipstack.com/{0}", ipAddress), query);

            (response, success) = await HttpClient.GetAsync<IpAddressDetailsDTO>(uri);
        }
        catch (Exception ex)
        {
            Debug.WriteLine($"is success: {ex.Message}{ex.StackTrace}");
        }

        return response;
    }
}

public interface IIpStackHandler
{
    Task<IpAddressDetailsDTO> GetGeodataAsync(string ipOrUri);
    IAppHttpClient HttpClient { get; }
    public string ApiKey { get; set; }
}
