﻿using Microsoft.EntityFrameworkCore;

namespace GeoSearch.IpStackManagement.Data;

public static class IpStackManagementContextExtensions
{
    public static async Task<IpAddressDetails?> IpAddressDetailsWithIpUri(this IpStackManagementContext context, IpOrUri ipUri, bool asNoTracking)
    {
        IQueryable<IpAddressDetails> query = context.Set<IpAddressDetails>();

        if (asNoTracking)
        {
            query = query.AsNoTracking();
        }
        //var ipAddressDetails = query.ToList();

        IpAddressDetails result = default;

        try
        {
            result = await(from ipDetails in context.IpAddressGeoDetails
                                     where ipDetails.IpOrUri == ipUri.Ipv4Url
                                     select ipDetails).FirstOrDefaultAsync();
        }
        catch (Exception ex)
        {
            Debug.WriteLine(string.Format("{0}{1}", ex.InnerException, ex.Message));
        }

        return result;

        //return context.IpAddressGeoDetails.Where(x => x.IpOrUri.Ipv4Url == ipUri.Ipv4Url).FirstOrDefaultAsync();

        //return Task.FromResult(ipAddressDetails.SingleOrDefault(x => x.IpOrUri.Ipv4Url == ipUri.Ipv4Url));
        //return context.IpAddressGeoDetails.SingleOrDefaultAsync(x => x.IpOrUri.Ipv4Url == ipUri.Ipv4Url);

        // -> this implementation is preferentional , it is not passing test 
        //return query.SingleOrDefaultAsync(x => string.Equals(x.IpOrUri.Ipv4Url, ipUri.Ipv4Url));
    }


    public static async Task<IpAddressDetails?> IpAddressDetailsWithId(this IpStackManagementContext context, int id, bool asNoTracking)
    {
        //IQueryable<IpAddressDetails> query = context.Set<IpAddressDetails>("IpAddressGeoDetails");

        //if (asNoTracking)
        //{
        //    query = query.AsNoTracking();
        //}

        IpAddressDetails result = default;

        try
        {
            result = await(from ipDetails in context.IpAddressGeoDetails
                           where ipDetails.Id == id
                           select ipDetails).FirstOrDefaultAsync();
        }
        catch (Exception ex)
        {
            Debug.WriteLine(string.Format("{0}{1}", ex.InnerException, ex.Message));
        }

        return result;

        //return context.IpAddressGeoDetails.SingleOrDefaultAsync(x => string.Equals(x.GetIdKey(), id));

        //return query.SingleOrDefaultAsync(x => EF.Property<int>(x, "Id") == id);
    }

    public static Task<List<IpAddressDetails>>? 
        GetAllIpAddressDetails(this IpStackManagementContext context, bool asNoTracking)
    {
        IQueryable<IpAddressDetails> query = context.Set<IpAddressDetails>("IpAddressGeoDetails");

        if (asNoTracking)
        {
            query = query.AsNoTracking();
        }

        return context.IpAddressGeoDetails.ToListAsync();

        return query.ToListAsync();
    }

}
