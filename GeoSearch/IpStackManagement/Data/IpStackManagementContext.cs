﻿
using Microsoft.EntityFrameworkCore;

namespace GeoSearch.IpStackManagement.Data;

public class IpStackManagementContext : DbContext, IIpStackManagementContext
{
    public IpStackManagementContext()  {  }
    public IpStackManagementContext(DbContextOptions<IpStackManagementContext> options) 
        : base(options) { }


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseSqlServer(
                "Server=(localdb)\\mssqllocaldb;Database=GeoSearchAppDb;Trusted_Connection=True;", 
                    sqlServerOptions =>
                    {
                        sqlServerOptions.CommandTimeout(60);
                        //sqlServerOptions.EnableRetryOnFailure();
                    });
#if DEBUG
            optionsBuilder.EnableSensitiveDataLogging();
#endif
        }
    }
    public virtual DbSet<IpAddressDetails> IpAddressGeoDetails { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //modelBuilder.Entity<IpAddressDetails>(x =>
        //{
        //    x.ToTable("IpAddressGeoDetails");
        //    x.Property<int>("Id");
        //    //x.Property(x => x.IpOrUri)
        //    //    .HasConversion(
        //    //        webTrgName => webTrgName, webTrgName => IpOrUri.Create(webTrgName));

        //    x.HasKey("Id");
        //});
    }


    public void MarkAsModified(IpAddressDetails item)
    {
        Entry(item).State = EntityState.Modified;
    }

}

public interface IIpStackManagementContext
{
    public DbSet<IpAddressDetails> IpAddressGeoDetails { get; }
    //int SaveChanges();
    //void MarkAsModified(Product item);
}

