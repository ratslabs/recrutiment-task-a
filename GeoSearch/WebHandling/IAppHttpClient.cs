﻿using System.Net;
using System.Net.Http;
using System.Text.Json;

namespace GeoSearch.WebHndling
{
    public class AppHttpClient : IAppHttpClient
    {
        public class SnakeCasePropertyNamingPolicy : JsonNamingPolicy
        {
            public override string ConvertName(string name)
            {
                return string.Concat(name.Select((character, index) =>
                        index > 0 && char.IsUpper(character)
                            ? "_" + character
                            : character.ToString()))
                    .ToLower();
            }
        }



        public AppHttpClient()
        {
            Client = new HttpClient();
            _serializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = new SnakeCasePropertyNamingPolicy(),
                WriteIndented = true,
            };
        }

        public HttpClient Client { get; protected set; }
        protected readonly JsonSerializerOptions _serializerOptions;
        public Action<string> OnRequestError { get; set; }


        /// <summary>
        /// This could be refactor to output TSuccess, TFail .
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="restUrl"></param>
        /// <returns></returns>
        public async Task<(TResponse, bool)> GetAsync<TResponse>(string restUrl)
        {
            TResponse payload = default;
            bool success = false;

            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, restUrl);

                
                using (HttpResponseMessage response = await Client.SendAsync(request))
                {
                    Debug.WriteLine(response.StatusCode);

                    if (!response.IsSuccessStatusCode)
                    {
                        OnRequestError?.Invoke(
                            string.Format("{type}::code{arg1}|uri{arg2}",
                                GetType().Name, response.RequestMessage, response.RequestMessage.RequestUri));
                    }
                    else
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        payload = JsonSerializer.Deserialize<TResponse>(content, _serializerOptions);
                        success = true;

                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"\tERROR {0}", $",{ex.Message}###{ex.StackTrace}");
            }

            return (payload, success);
        }
    }


    public interface IAppHttpClient
    {
        Task<(TResponse, bool)> GetAsync<TResponse>(string restUrl);
        HttpClient Client { get; }
        Action<string> OnRequestError { get; set; }
    }
}
