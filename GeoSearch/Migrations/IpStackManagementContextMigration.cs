﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GeoSearch.Migrations;

[Migration("1_VehicleContextmigration")]
[DbContext(typeof(IpStackManagementContext))]
public class IpStackManagementContextMigration : Migration
{
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "IpAddressGeoDetails",
            columns: table => new
            {
                Id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                IpOrUri = table.Column<string>(type: "nvarchar(max)", nullable: true),
                Hostname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                ContinentCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                ContinentName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                CountryCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                CountryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                RegionCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                RegionName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                City = table.Column<string>(type: "nvarchar(max)", nullable: true),
                Zip = table.Column<string>(type: "nvarchar(max)", nullable: true),
                Latitude = table.Column<double>(type: "float", nullable: false),
                Longitude = table.Column<double>(type: "float", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_IpAddressGeoDetails", x => x.Id);
            });

//        migrationBuilder.CreateTable(
//                Ip = table.Column<string>(nullable: true),
//            },

        //migrationBuilder.CreateTable(
        //    name: "Locations",
        //    columns: table => new
        //    {
        //        Id = table.Column<int>(type: "int", nullable: false)
        //            .Annotation("SqlServerIdentity", "1, 1"),

        //        GeonameId = table.Column<string>(type: "string", nullable: false),
        //        Capital = table.Column<string>(type: "string", nullable: false),
        //        //Languages = table.Column<string>(type: "string", nullable: false),
        //        CountryFlag = table.Column<string>(type: "string", nullable: false),
        //        CountryFlagEmoji = table.Column<string>(type: "string", nullable: false),
        //        CountryFlagEmojiUnicode = table.Column<string>(type: "string", nullable: false),
        //        CallingCode = table.Column<string>(type: "string", nullable: false),
        //        IsEu = table.Column<bool>(type: "bool", nullable: false),
        //    },
        //    constraints: table =>
        //    {
        //        table.PrimaryKey("PK_Location", x => x.Id);
        //    });
        //migrationBuilder.CreateTable(
        //    name: " ")
    }


    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable("IpAddressGeoDetails");
        //migrationBuilder.DropTable("Locations");
    }

}
