﻿using GeoSearch.IpStackManagement;
using Splat;

namespace GeoSearch.Views.Models;

public class IpDetailsViewModel : ReactiveObject
{
    private async Task SaveGeodataToDb()
    {
        var addIpUriAddresDetailsResult 
            = await _geolocalisationService.AddIpAddressDetailsAsync(IpAddressDetails);

        if (addIpUriAddresDetailsResult.IsSuccess is false)
        {
            OnStateChange.Invoke(addIpUriAddresDetailsResult.Error.ToString());
        }

        var ipAddressDetailsResult = await _geolocalisationService.GetByIpUriAsync(IpAddressDetails.IpOrUri);
        if (ipAddressDetailsResult.IsSuccess is false)
        {
            OnStateChange.Invoke(ipAddressDetailsResult.Error.ToString());
        }
        else
        {
            IpAddressDetails = ipAddressDetailsResult.Value;
            IsSavedInDb = true;
        }
    }

    private async Task DeleteIpGeodataFromDb()
    {
        var deleteResult = await _geolocalisationService.DeleteAddressDetailsByIdAsync(_ipAddressDetails.Id);
        if (deleteResult.IsSuccess is false)
        {
            OnStateChange.Invoke(string.Format("{0}", deleteResult.Error));
        }
        else
        {
            onDeleteItem.Invoke(this);
        }
    }


    // -> is this a data provider??;
    IGeolocalisationService _geolocalisationService { get; } 
        = Locator.Current.GetService<IGeolocalisationService>();

    public ReactiveCommand<Unit, Task> SaveToDbCommand { get; }
    public ReactiveCommand<Unit, Task> DeleteFromDbCommand { get; }

    public Action<IpDetailsViewModel> onDeleteItem { get; set; }
    public Action<string> OnStateChange { get; set; }


    public IpDetailsViewModel(IpAddressDetails ipAddressDetails)
    {
        SaveToDbCommand = ReactiveCommand.Create(SaveGeodataToDb);
        DeleteFromDbCommand = ReactiveCommand.Create(DeleteIpGeodataFromDb);
        IpAddressDetails = ipAddressDetails;
    }

    private bool _dbSave;
    public bool IsSavedInDb
    {
        get => _dbSave;
        set => this.RaiseAndSetIfChanged(ref _dbSave, value);
    }


    private IpAddressDetails _ipAddressDetails;
    public IpAddressDetails IpAddressDetails
    {
        get => _ipAddressDetails;
        set => this.RaiseAndSetIfChanged(ref _ipAddressDetails, value);
    }



    public string TargetName => _ipAddressDetails.IpOrUri;
    public string ContinentName => _ipAddressDetails.ContinentName;
    public string RegionName => _ipAddressDetails.RegionName;
    public string CountryName => _ipAddressDetails.CountryName;
    public string City => _ipAddressDetails.City;
    public double Latitude => Math.Round(_ipAddressDetails.Latitude, 5);
    public double Longitude => Math.Round(_ipAddressDetails.Longitude, 5);




}
