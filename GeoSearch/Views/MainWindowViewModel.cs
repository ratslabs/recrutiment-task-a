﻿using System.Reactive.Linq;
using ReactiveUI.Fody.Helpers;
//using Windows.Media.Protection.PlayReady;
using Mapster;
using DynamicData;
using Splat;
using GeoSearch.IpStackManagement.DTOs;

namespace GeoSearch.Views.Models;

public class MainWindowViewModel : ReactiveObject
{
    /// <summary> window factory </summary> 
    private IpDetailsViewModel WindowFactory_GreateGeolocalizationWindows(
        IpAddressDetails unsavedIpAddresDetails, bool haveSave)
    {
        var ipAddresViewModel = new IpDetailsViewModel(unsavedIpAddresDetails);

        ipAddresViewModel.onDeleteItem += RefreshIpDetalisList;
        ipAddresViewModel.OnStateChange += SetStatusMsg;
        ipAddresViewModel.IsSavedInDb = haveSave;

        return ipAddresViewModel;
    }

    /// <summary> removes IpDetailsViewModel form the ui </summary>
    private void RefreshIpDetalisList(IpDetailsViewModel viewModel) => ObservableGeolocalisationList.Remove(viewModel);

    private void SetStatusMsg(string value) => StatusMessage = value;


    public MainWindowViewModel()
    {
        _ipStackHandler = Locator.Current.GetService<IIpStackHandler>();
        _geoService = Locator.Current.GetService<IGeolocalisationService>();

        GetGeoLocalisationCommand = ReactiveCommand.Create(FindIpGeolocation);

        Task.Run(async () => { 

            var result = await _geoService.GetAllAsync();
            result.Value.ToList()
                .ForEach(ipDetails =>
                {
                    var detailsWindow = WindowFactory_GreateGeolocalizationWindows(ipDetails, true);
                    _ipFoundSourceList.Add(detailsWindow);
                });

        }).Wait();

        _ipFoundSourceList.Connect().ObserveOn(RxApp.MainThreadScheduler).Bind(ObservableGeolocalisationList).Subscribe();
        this.WhenAnyValue(vm => vm.ApiKey).Subscribe(apiKey =>
        {
            _ipStackHandler.ApiKey = apiKey;
        });
    }


    public ReactiveCommand<Unit, Task> GetGeoLocalisationCommand { get; }

    private readonly IIpStackHandler _ipStackHandler;
    private readonly IGeolocalisationService _geoService;
    
    [Reactive] public string ApiKey { get; set; } = "ee14a3ba51d54fa8e581a038bdccef63";
    [Reactive] public string StatusMessage { get; set; } = string.Empty;


    public IObservableCollection<IpDetailsViewModel> ObservableGeolocalisationList { get; set; }
        = new ObservableCollectionExtended<IpDetailsViewModel>();
    private SourceList<IpDetailsViewModel> _ipFoundSourceList = new SourceList<IpDetailsViewModel>();


    private string _targetIpUri;
    public string IpOrUrl
    {
        get => _targetIpUri;
        set => this.RaiseAndSetIfChanged(ref _targetIpUri, value);
    }


    public async Task FindIpGeolocation()
    {
        if (IpOrUri.TryCreate(IpOrUrl, out var webTargetName) is false)
        {
            StatusMessage = $"{IpOrUrl} is not a valid IP4 or URL .";
            return;
        }


        IpAddressDetailsDTO ipAddressDetailsDto = await _ipStackHandler.GetGeodataAsync(IpOrUrl);

        if (ipAddressDetailsDto is not null && ipAddressDetailsDto.Ip is null)
        {
            /// -> we should expect an error or warning message instead of an empty Ip altrough it works
            StatusMessage = "No results ! Is an API key still valid ?";
            return;
        }
        var newIpAddressDetails = IpAddressDetails.Create(webTargetName);

        ipAddressDetailsDto.Adapt(newIpAddressDetails);

        var ipAddresDetailsView = WindowFactory_GreateGeolocalizationWindows(newIpAddressDetails, false);


        var uiGelocalizationList = new List<IpDetailsViewModel> { ipAddresDetailsView };
        ObservableGeolocalisationList.Add(uiGelocalizationList);
        
        StatusMessage = string.Format("New geodata result for avialable for: {0}", ipAddressDetailsDto.Ip);
    }




}
