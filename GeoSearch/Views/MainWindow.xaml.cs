﻿
using GeoSearch.Views.Models;
using System.Reactive.Disposables;

namespace GeoSearch.Views;

public partial class MainWindow
{
    public MainWindow()
    {
        InitializeComponent();
        ViewModel = new MainWindowViewModel();

        this.WhenActivated(disposableRegistration =>
        {
            this.BindCommand(ViewModel, vm => vm.GetGeoLocalisationCommand, view => view.RequestGeodata_button).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, vm => vm.ObservableGeolocalisationList, view => view.ipGeodataList.ItemsSource).DisposeWith(disposableRegistration);
            this.Bind(ViewModel, vm => vm.IpOrUrl, view => view.ipUrlSearchBox.Text) .DisposeWith(disposableRegistration);
            this.Bind(ViewModel, vm => vm.ApiKey, view => view.apiKeyBox.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, vm => vm.StatusMessage, view => view.statusMessage_Text.Text).DisposeWith(disposableRegistration);
        });
    }


}
