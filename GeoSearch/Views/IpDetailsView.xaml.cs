﻿
using System.Reactive.Disposables;
using System.Windows;

namespace GeoSearch.Views;


public partial class IpDetailsView
{
    public IpDetailsView()
    {
        InitializeComponent();
        this.WhenActivated(disposableRegistration =>
        {
            this.OneWayBind(ViewModel, viewModel => viewModel.TargetName, view => view.IpAddress.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.ContinentName, view => view.ContinentName.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.CountryName, view => view.CountryName.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.RegionName, view => view.RegionName.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.City, view => view.City.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.Longitude, view => view.Latitude.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.Latitude, view => view.Longitude.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.Latitude, view => view.Longitude.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.Latitude, view => view.Longitude.Text).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.IsSavedInDb, view => view.saveToDb_button.Visibility,
                isVisible => isVisible ? Visibility.Collapsed : Visibility.Visible).DisposeWith(disposableRegistration);
            this.OneWayBind(ViewModel, viewModel => viewModel.IsSavedInDb, view => view.deleteFromDb_button.Visibility,
                isVisible => isVisible ? Visibility.Visible : Visibility.Collapsed).DisposeWith(disposableRegistration);
            this.BindCommand(ViewModel, viewModel => viewModel.SaveToDbCommand, view => view.saveToDb_button).DisposeWith(disposableRegistration);
            this.BindCommand(ViewModel, viewModel => viewModel.DeleteFromDbCommand, view => view.deleteFromDb_button).DisposeWith(disposableRegistration);
        });
    }

    private void refresh_button_Click(object sender, RoutedEventArgs e)
    {

    }
}
